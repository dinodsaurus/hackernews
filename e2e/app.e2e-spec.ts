import { HackernewsPage } from './app.po';

describe('hackernews App', () => {
  let page: HackernewsPage;

  beforeEach(() => {
    page = new HackernewsPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
