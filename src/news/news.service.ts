import { Injectable }    from '@angular/core';
import { Http } from '@angular/http';
import { Article } from "./news";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class NewsService {
    constructor(private http: Http){}
    getNews() {
        return this.http.get("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
            .toPromise()
            .then(response => response.json())
    }
    getSingleNews(id: number) {
        return this.http.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
            .toPromise()
            .then(response => response.json() as Article);
    }
}

