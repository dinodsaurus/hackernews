import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NewsComponent } from './news.component';
import { ArticleComponent } from './article/article.component';
import { NewsService } from './news.service'

@NgModule({
  declarations: [
    NewsComponent,
    ArticleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ NewsService ],
  exports: [
    NewsComponent
  ]
})
export class NewsModule {}
