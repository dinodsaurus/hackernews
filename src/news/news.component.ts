import { Component } from '@angular/core';
import { NewsService } from './news.service';
import { Article } from './news';
@Component({
  selector: 'news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})

export class NewsComponent {
  news:Article[] = [];
    constructor(private newsService: NewsService){
        this.newsService.getNews().then((news: number[]) => news.map( (n:number) => {
            this.newsService.getSingleNews(n).then( (n:Article) => {
                this.news.push(n)
            });
        }));
    }
}
